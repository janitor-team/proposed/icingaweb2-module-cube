<?php
// Icinga Web 2 Cube Module | (c) 2016 Icinga GmbH | GPLv2

$this->provideHook('cube/Actions', 'Cube/MonitoringActions');
$this->provideHook('cube/IcingaDbActions', 'Cube/IcingaDbActions');

$this->provideHook('icingadb/icingadbSupport');
